/**
1. Create an activity.js file on where to write and save the solution for the activity.
[done]2. Find users with letter s in their first name or d in their last name.
Use the $or operator.
Show only the firstName and lastName fields and hide the _id field.
[done]3. Find users who are from the HR department and their age is greater than or equal to 70.
Use the $and operator
[done]4. Find users with the letter e in their first name and has an age of less than or equal to 30.
Use the $and, $regex and $lte operators
5. Create a git repository named s29.
 */



//find the letter s in the first name or d in the last name in the users collection

db.users.find({
    $or: [{firstName: {$regex: 'S', $options: '$i'}}, {lastName: {$regex: 'D', $options: '$i'}}]
},{
    firstName: 1,
    lastName: 1,
    _id: 0
});

//find users from HR department and their age >= 70

db.users.find({
    $and: [{department: "HR"}, {age: {$gte: 70}}]
});

//users with letter e in their first name and with age <=30

db.users.find({
    $and: [{firstName: {$regex: 'e', $options: '$i'}}, {age: {$lte: 30}}]
});